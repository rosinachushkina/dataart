﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Selenium;
using Selenium.Pages;
using System.Threading;

namespace Selenuim.Pages
{
    public class MenuPage: BasePage
    {
        public MenuPage(IWebDriver driver) : base(driver)
        {

        }
        public const string MusicTabXpath = "//*[text()='Music']";
        public const string JazzTabXpath = "//*[text()='Jazz']";

        public const string ModernTabXpath = "//*[contains(text(),'Modern')]";
        internal void ClickMusic()
        {
            Driver.GetByPath(MusicTabXpath).Click();
            
        }
        internal void ClickJazz()
        {
            Actions action = new Actions(Driver);
            ((IJavaScriptExecutor)Driver).ExecuteScript("window.scrollTo(0,100);");
            action.MoveToElement(Driver.GetByPath(JazzTabXpath)).Perform();

        }
        internal void ClickModern()
        {
            Actions action = new Actions(Driver);
            ((IJavaScriptExecutor)Driver).ExecuteScript("window.scrollTo(0,100);");
            action.MoveToElement(Driver.GetByPath(ModernTabXpath)).Perform();

        }
    }
}
