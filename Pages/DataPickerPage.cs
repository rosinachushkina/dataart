﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Selenuim.Pages;



namespace Selenium.Pages
{
    public class DataSelectorPage: BasePage
    {
        public const string SelectPickerXpath = "//input[@id='datepicker']";
        public const string ScriptXpath = "//div[2]/script[3]";

        public const string PreviousMonthXpath = "//*[@class='ui-icon ui-icon-circle-triangle-w']";
        public const string DataSelectorXpath = "//*[@id='ui-datepicker-div']//a[text()='{0}']";
        



        public DataSelectorPage(IWebDriver driver) : base(driver)
        {

        }
        
        public DataSelectorPage ClickPicker()
        {
            Driver.GetByPath(SelectPickerXpath).Click();
            return this;
        }
        public DataSelectorPage ClickPrevioseMonth()
        {
            Driver.GetByPath(PreviousMonthXpath).Click();
            return this;
        }

        public string SetDate
        {
            set

            {

                Driver.GetByPath(SelectPickerXpath).SendKeys(value);
            }
        }
        internal void ClearDatePickerInput()

        {
            Driver.GetByPath(SelectPickerXpath).Clear();


        }

        //internal void RunScript()

        //{
        //    Actions action = new Actions(Driver);
        //    ((IJavaScriptExecutor)Driver).ExecuteScript("return document.datepicker;");

        //}
        //public string AddeddateValue
        //{
        //    get
        //    {

        //        return Driver.GetByPath(ScriptXpath).Text;



        //    }
        //}

        internal bool IsSaveDate(string date)
        {
            var elementClass = Driver.GetByPath(string.Format(SelectPickerXpath, date)).GetAttribute("value");
            return elementClass.Contains("2019");
        }
    }
}

