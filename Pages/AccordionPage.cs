﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace Selenium.Pages
{
    public class AccordionPage : BasePage
    {
        public const string GlobalSectionPath = "//div/h3";
        

        private IWebDriver driver;
        public AccordionPage(IWebDriver currentDriver) : base(currentDriver)
        {
            driver = currentDriver;
        }



        public AccordionPage ClickSection(byte sectionNumber)
        {
            Driver.GetByPath($"{GlobalSectionPath}[{sectionNumber}]").Click();
            return this;
        }

        public string GetTextFromSection(byte sectionNumber)
        {
            return Driver.GetByPath($"{GlobalSectionPath}[{sectionNumber}]/following-sibling::div[1]/p").Text;
        }
    }
}
