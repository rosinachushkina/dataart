﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Selenium;
using Selenium.Framework;
using Selenium.Pages;
using System;
using System.Threading;

namespace Selenuim.Pages
{
    public class TooltipandDoubleclick : BasePage
    {
        public const string DoubleclickXpath = "//button[@id='doubleClickBtn']";
        public const string RightclicXpath = "//button[@id='rightClickBtn']";
        public const string contentXpath = "//div[@id='rightClickBtn']";
        public const string CopyMeXpath = "//*[text()='Copy Me']";
        public const string tooltXpath = "//div[@id='tooltipDemo']";
        public const string tooltmessageXpath = "//span[@class='tooltiptext']";






        public TooltipandDoubleclick(IWebDriver driver) : base(driver)
        {
            
        }
        internal void Doubleclick()
        {
            Actions action = new Actions(Driver);
            var findelement = Driver.GetByPath(DoubleclickXpath);
            action.DoubleClick(findelement).Build().Perform();
          
        }
        public string AlertDoubleclickText
        {
            get
            {
                return Driver.SwitchTo().Alert().Text;
            }
        }
      

        internal void Rightclickclick()
        {
            Actions action = new Actions(Driver);
            var findelement = Driver.GetByPath(RightclicXpath);
            action.ContextClick(findelement).Build().Perform();
          
        }
        internal void Copymeclick()
        {
            Actions action = new Actions(Driver);
            ((IJavaScriptExecutor)Driver).ExecuteScript("window.scrollTo(0,100);");
            var findelement = Driver.GetByPath(CopyMeXpath);

            action.Click(findelement).Build().Perform();

        }
        public string AlertText
        {
            get
            {
                return Driver.SwitchTo().Alert().Text;
            }
        }

        

        internal void AcceptAlert()
        {
            Driver.WaitForAlert(2).Accept();

        }
       
        internal void ClickHoverTooltip()
        {

            Driver.GetByPath(tooltXpath).Click();
            
            

        }
        internal void ReadHoverTooltip()
        {

            Actions action = new Actions(Driver);
            var findelement = Driver.GetByPath(tooltXpath);
            action.MoveToElement(findelement).Build().Perform();



        }
        
        public string GetPass
        {
            get
            {
                return Driver.GetByPath(tooltmessageXpath).Text;
            }
        }

        

    }
}
