﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Data;
using System.Threading;
using System.Windows.Forms;

namespace Selenium.Pages
{
    public class AutocompletePage
    {
        public const string EnterSymbolInTagsFielInputeXpath = "//*[@id='tags']";
        public const string TagsInputXpath = "//input[@id='tags']";
        public const string TagDropDownListXPath = "//div[contains(text(),'{0}')]";

        



        private IWebDriver driver;

        public AutocompletePage(IWebDriver currentDriver)
        {
            driver = currentDriver;
        }
        public string EnterSymbolInTagsFiel
        {
            set

            {

                driver.GetByPath(EnterSymbolInTagsFielInputeXpath).SendKeys(value);
                Thread.Sleep(500);
            }
        }
        
        
        internal void ClickTagField()
        {
            driver.GetByPath(EnterSymbolInTagsFielInputeXpath).Click();

        }
        internal void SelectTagFromAutocomplete(string tag)
        {
            driver.GetByPath(string.Format(TagDropDownListXPath, tag)).Click();
        }




    }
}
