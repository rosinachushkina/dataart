﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace Selenium.Pages
{
    public class CheckboxradioPage : BasePage
    {
        public const string SelectByNameXpath = "//*[contains(text(),'{0}')]";



        public CheckboxradioPage(IWebDriver driver) : base(driver)
        {

        }
        internal void SelectLocation(string location)
        {
            Driver.GetByPath(string.Format(SelectByNameXpath, location)).Click();
        }
        internal void SelectRating(string rating)
        {
            Driver.GetByPath(string.Format(SelectByNameXpath, rating)).Click();
        }
        internal void SelectType(string type)
        {
            Driver.GetByPath(string.Format(SelectByNameXpath, type)).Click();
        }


            internal bool IsLocationActive(string location)
        {
            var elementClass = Driver.GetByPath(string.Format(SelectByNameXpath, location)).GetAttribute("class");
            return elementClass.Contains("ui-state-active");
        }
         
        internal bool IsRatingsActive(string rating)
        {
            var elementClass = Driver.GetByPath(string.Format(SelectByNameXpath, rating)).GetAttribute("class");
            return elementClass.Contains("ui-state-active");
        }
        internal bool IsTypeActive(string location)
        {
            var elementClass = Driver.GetByPath(string.Format(SelectByNameXpath, location)).GetAttribute("class");
            return elementClass.Contains("ui-state-active");
        }

    }
}
