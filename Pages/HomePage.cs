using System;
using OpenQA.Selenium;
using Selenuim.Pages;

namespace Selenium.Pages
{
    public class HomePage : BasePage
    {
        public const string HomeHeaderxpath = "//h1[@class='entry-title']";
        public const string KeyboardSamplePageButtonPath = "//*[text()='Keyboard Events Sample Form']";
        public const string MenuPageButtonPath = "//*[text()='Menu']";
        public const string AutocompletePageButtonPath = "//*[text()='Autocomplete']";
        public const string AccordionPageButtonPath = "//*[text() = 'Accordion']";
        public const string CheckboxradioPageButtonPath = "//*[text()='Checkboxradio']";
        public const string DataPickerPageButtonPath = "//*[text()='Datepicker']";
        public const string TooltipandDoubleclickButtonPath = "//*[text()='Tooltip and Double click']";



        public HomePage(IWebDriver driver) : base(driver)
        {

        }

        public string GetHomePageHeader
        {
            get
            {
                return Driver.GetByPath(HomeHeaderxpath).Text;
            }
        }

        public KeyboardEventsSampleFormPage ClickKeyboardEventSamplePage()
        {
            Driver.GetByPath(KeyboardSamplePageButtonPath).Click();
            return new KeyboardEventsSampleFormPage(Driver);       
        }
        public MenuPage ClickMenuePage()
        {
            Driver.GetByPath(MenuPageButtonPath).Click();
            return new MenuPage(Driver);
        }
        public AutocompletePage ClickAutocpmpletePage()
        {
            Driver.GetByPath(AutocompletePageButtonPath).Click();
            return new AutocompletePage(Driver);
        }
        public AccordionPage ClickAccordionPage()
        {
            Driver.GetByPath(AccordionPageButtonPath).Click();
            return new AccordionPage(Driver);
        }

        public CheckboxradioPage ClickCheckboxradioPage()
        {
            Driver.GetByPath(CheckboxradioPageButtonPath).Click();
            return new CheckboxradioPage(Driver);
        }
        public DataSelectorPage ClickDataSelectorPage()
        {
            Driver.GetByPath(DataPickerPageButtonPath).Click();
            return new DataSelectorPage(Driver);
        }
        public TooltipandDoubleclick ClickTooltipandDoubleclick()
        {
            Driver.GetByPath(TooltipandDoubleclickButtonPath).Click();
            return new TooltipandDoubleclick(Driver);
        }
        
    }
}