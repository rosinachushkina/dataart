﻿using NUnit.Framework;
using OpenQA.Selenium;


namespace Selenium.Pages
{ 
        public class KeyboardEventsSampleFormPage : BasePage
        {
            public const string NameInputXpath = "//input[@id='userName']";
            public const string CurrentAddressInputXpath = "//textarea[@id='currentAddress']";
            public const string PermanentAddressInputpath = "//textarea[@id='permanentAddress']";
            public const string SubmitButtonpath = "//input[@id='submit']";


        public KeyboardEventsSampleFormPage(IWebDriver driver) : base(driver)
        {

        }


        public string SetNameInput
            {
                set
                {
                    Driver.GetByPath(NameInputXpath).SendKeys(value);
                }
            }
            public string SetCurrentAddressInput
            {
                set
                {
                Driver.GetByPath(CurrentAddressInputXpath).SendKeys(value);
                }
            }
            public string SetPermanentAddressInput
            {
                set
                {
                Driver.GetByPath(PermanentAddressInputpath).SendKeys(value);
                }
            }
            internal void SubmitButton()
            {
            Driver.GetByPath(SubmitButtonpath).Click();
                
            }
        public string AlertText
        {
            get
            {
                return   Driver.SwitchTo().Alert().Text;
            }
        }
        

        internal void AcceptAlert()
        {

            Driver.WaitForAlert(2).Accept();
        }

    }
}
