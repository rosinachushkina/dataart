﻿using System;
using System.Text;

namespace Selenium
{
    public static class Generators
    {
        public static string RandomStringLatin(int length)
        {
            //set for the random string 
            string chars = "abcdefghijklmnopqrstuvwxyz";
            StringBuilder builder = new StringBuilder(length);

            var random = new Random(Environment.TickCount);
            for (int i = 0; i < length; ++i)
                builder.Append(chars[random.Next(chars.Length)]);

            return builder.ToString();
        }
    }
}
