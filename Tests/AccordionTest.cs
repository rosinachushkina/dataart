﻿using NUnit.Framework;
using Selenium.Framework;
using Selenium.Pages;
using System;
using System.Collections.Generic;

namespace Selenium
{
    class AccordionTest :BaseTest
    {
        public static readonly string AccordiontitleER = "Accordion";
        public const string AccumulateXpath = "//div[@id='ui-id-[{0}]]";
        public const string AccumulateClickXpath = "//div/h3[1][{0}]";

        //Dictionary(key/value)
//here should be multy assert
        private Dictionary<byte, string> AccordionSections = new Dictionary<byte, string>
        {
            [1] = "Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, " +
            "condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.",
            [2] = "Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus " +
            "interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.",
            [3] = "Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. " +
            "Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.",
            [4] = "Cras dictum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum ante ipsum primis in faucibus orci " +
            "luctus et ultrices posuere cubilia Curae; Aenean lacinia mauris vel est.",
        };
   
        [Test]
        public void Accordion()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);

          
            var accordionPage = homePage.ClickAccordionPage();

            foreach(var pair in AccordionSections)
            {
                accordionPage.ClickSection(pair.Key);
                var text = accordionPage.GetTextFromSection(pair.Key);

                Assert.True(text.Contains(pair.Value));
            }
        }
    }
}
