﻿using NUnit.Framework;
using Selenium.Framework;
using Selenium.Pages;

namespace Selenium
{
    public class AutocompleteTest : BaseTest
    {
        public static readonly string EnterSymbolInTagsFielER = "P";
        public static readonly string ValueFromAutocompleteListER = "Perl";
        //public static readonly string EnterPSymbolInTagsFielERE = "E";
        //public static readonly string ValueFromAutocompleteListERE = "Erlang";

        [Test]
        public void Autocomplete()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);

            // Assert Header
            var autocompletePage = homePage.ClickAutocpmpletePage();
            autocompletePage.ClickTagField();
            autocompletePage.EnterSymbolInTagsFiel = EnterSymbolInTagsFielER;
            autocompletePage.SelectTagFromAutocomplete(EnterSymbolInTagsFielER);
            string GetPass = Driver.GetByPath(AutocompletePage.EnterSymbolInTagsFielInputeXpath).GetAttribute("value");
            Assert.That(GetPass, Is.EqualTo(ValueFromAutocompleteListER));
        }
    }
}
