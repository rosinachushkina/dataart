﻿using NUnit.Framework;
using Selenium.Framework;


namespace Selenium
{
    class Menu: BaseTest
    {
        [Test]
        public void MenuTest()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);

            var menuPage = homePage.ClickMenuePage();
                       
            menuPage.ClickMusic();
            menuPage.ClickJazz();
            menuPage.ClickModern();
            
        }
    }
}
