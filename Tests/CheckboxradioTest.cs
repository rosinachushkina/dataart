﻿using NUnit.Framework;
using Selenium.Framework;
using Selenium.Pages;
using System;
using System.Collections.Generic;

namespace Selenium
{
    public class CheckboxradioTest :BaseTest
    {
        [Test]
        public void Checkboxradio()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);


            var checkboxradioPage = homePage.ClickCheckboxradioPage();


            var locations = new string[] { "London", "Paris" };

            foreach (var location in locations)
            {
                checkboxradioPage.SelectLocation(location);
                var londonActive = checkboxradioPage.IsLocationActive(location);
                Assert.True(londonActive);
            }
            var ratings = new string[] { "2 Star", "3 Star" };
            foreach (var rating in ratings)
            {
                checkboxradioPage.SelectLocation(rating);
                var ratingisactive = checkboxradioPage.IsLocationActive(rating);
                Assert.True(ratingisactive);
            }
            var types = new string[] { "2 Queen", "1 King" };
            foreach (var type in types)
            {
                checkboxradioPage.SelectType(type);
                var typeisactive = checkboxradioPage.IsLocationActive(type);
                Assert.True(typeisactive);
            }
        }

        [Test]
        public void CheckboxradioUnchecked()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);


            var checkboxradioPage = homePage.ClickCheckboxradioPage();


            var locations = new string[] { "London", "Paris" };
            foreach (var location in locations)
            {
                var londonActive = checkboxradioPage.IsLocationActive(location);
                Assert.False(londonActive);
            }
            var ratings = new string[] { "2 Star", "3 Star" };
            foreach (var rating in ratings)
            {
                var ratingisactive = checkboxradioPage.IsLocationActive(rating);
                Assert.False(ratingisactive);
            }
            var types = new string[] { "2 Queen", "1 King" };
            foreach (var type in types)
            {
                var typeisactive = checkboxradioPage.IsLocationActive(type);
                Assert.False(typeisactive);
            }

        }
    }
}
