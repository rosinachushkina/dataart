﻿using NUnit.Framework;
using Selenium.Framework;
using Selenuim.Pages;

namespace Selenium
{
    public class TooltipandDoubleclickTest : BaseTest
    {
        
        [Test]
        public void TooltipandDoubleclickt()
        {
             
            string AlerttextER = "You have selected Copy";
            string ValueFromAutocompleteListER = "We ask for your age only for statistical purposes.";
            string AlertDoubleClickER = "Hi,You are seeing this message as you have double cliked on the button";

            

        var homePage = SiteNavigator.NavigateToHomePagee(Driver);
            var tooltipanddoubleclick = homePage.ClickTooltipandDoubleclick();
            tooltipanddoubleclick.Doubleclick();
            Assert.IsTrue(tooltipanddoubleclick.AlertDoubleclickText.Contains(AlertDoubleClickER));
            tooltipanddoubleclick.AcceptAlert();
            tooltipanddoubleclick.Rightclickclick();
            tooltipanddoubleclick.Copymeclick();
            Assert.IsTrue(tooltipanddoubleclick.AlertText.Contains(AlerttextER));
            tooltipanddoubleclick.AcceptAlert();
            tooltipanddoubleclick.ClickHoverTooltip();
            tooltipanddoubleclick.ReadHoverTooltip();
            Assert.That(tooltipanddoubleclick.GetPass, Is.EqualTo(ValueFromAutocompleteListER));





        }
    }
}
