﻿using NUnit.Framework;
using Selenium.Framework;

namespace Selenium
{
    public class KeyboardTest : BaseTest
    {
        public static readonly string HomeHeaderER = "Home";
        public static readonly string CurrentAddressInputeER = "CurrentAddress";
        public static readonly string PermanentAddressER = "PermanentAddress";
        public static readonly string AlertTextER = "Thanks for submitting the information";

        [Test]
        public void Keyboard()
        {
            var homePage = SiteNavigator.NavigateToHomePagee(Driver);

           
            var keyboardPage = homePage.ClickKeyboardEventSamplePage();

            var name = Generators.RandomStringLatin(6);

            keyboardPage.SetNameInput = name;
            keyboardPage.SetCurrentAddressInput = CurrentAddressInputeER;
            keyboardPage.SetPermanentAddressInput = PermanentAddressER;
            keyboardPage.SubmitButton();
            Assert.IsTrue(keyboardPage.AlertText.Contains(AlertTextER));
            keyboardPage.AcceptAlert();

        }

    }
}
