﻿using NUnit.Framework;
using Selenium.Framework;
using Selenium.Pages;
using System.Threading;

namespace Selenium
{
   public class DataPickerTest: BaseTest
    {
        


        
        [Test]
        public void DataPicker()
        {

            var homePage = SiteNavigator.NavigateToHomePagee(Driver);
            var datapicker = homePage.ClickDataSelectorPage();
            datapicker.ClickPicker();
            
            datapicker.ClickPrevioseMonth();

            var previousdates = new string[] { "07/05/2019", "07/01/2019" };

            foreach (var previousdate in previousdates)
                {
                 datapicker.SetDate =previousdate;

                var issavedate = datapicker.IsSaveDate(previousdate);
                    Assert.True(issavedate);
                datapicker.ClearDatePickerInput();

            }




        }



        }

    }

