using System.Configuration;
using OpenQA.Selenium;
using Selenium.Pages;

namespace Selenium.Framework
{
    public class SiteNavigator
    {
        public static HomePage NavigateToHomePagee(IWebDriver driver)
        {
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["baseUrl"]);
            return new HomePage(driver);
        }
        //public static HomePage NavigateToHomePagee(IWebDriver driver)
        //{
        //    driver.Navigate().GoToUrl("https://demoqa.com/");
        //    return new HomePage(driver);
        //}
    }
}