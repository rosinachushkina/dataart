﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace Selenium
{
    public static class WebDriverExtensions
    {
        public static IWebElement GetByPath(this IWebDriver webDriver, string xpath)
        {
            return webDriver.FindElement(By.XPath(xpath));
        }

        public static IAlert WaitForAlert(this IWebDriver driver, int timeoutInSeconds)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));

            Func<IWebDriver, bool> waitUntilAlertIsDisplayed = new Func<IWebDriver, bool>((IWebDriver webDriver) =>
            {
                try
                {
                    driver.SwitchTo().Alert();
                }
                catch (NoAlertPresentException)
                {
                    return false;
                }
                return true;
            });

            wait.Until(waitUntilAlertIsDisplayed);
            return driver.SwitchTo().Alert();
        }

       
    }
}
